
const routes = [
  {
    path: '/',
    name: '/',
    component: () => import('pages/FrontendExercise/DashboardPage.vue'),
    children: [
      {
        path: "my-table",
        name: "my-table",
        component: () => import("pages/FrontendExercise/TablePage.vue"),
      },
      {
        path: "my-form",
        name: "my-form",
        component: () => import("pages/FrontendExercise/FormPage.vue"),
      },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
