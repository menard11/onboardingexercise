export default {
    data(){
        return{
            showDashboard: true
        }
    },
    watch: {
    $route(to, from) {
    if (from.name === 'my-table' && to.name === '/') {
      this.showDashboard = true; // Reset the value only if on the dashboard route
     } 
    },
   },
    
    methods: {
        toggledashboard(){
            this.showDashboard = false;
        }
    }
}