import { onMounted, ref } from "vue";
import axios from "axios";
import Swal from "sweetalert2";
import { useRouter } from "vue-router";

export default {
  setup() {
    const router = useRouter();

    const formData = ref({
      name: "",
      EmployeeID: "",
      email: "",
      status: "",
      address: "",
      description: "",
      currentTask: "",
    });

    const submitForm = async () => {
      try {
        if (router.currentRoute.value.query.id) {
          const response = await axios.put(
            `http://localhost:3000/Employee/${id}`,
            formData.value
          );
          console.log("Data updated successfully:", response.data);
          Swal.fire({
            icon: "success",
            title: "Success!",
            text: "Data updated successfully!",
          });
          router.push({ name: "my-table" });
        } else {
          const response = await axios.post(
            "http://localhost:3000/Employee",
            formData.value
          );
          console.log("Data added successfully:", response.data);
          formData.value = {};
          Swal.fire({
            icon: "success",
            title: "Success!",
            text: "Data added successfully!",
          });
          router.push({ name: "my-table" });
        }
      } catch (error) {
        console.error("Error adding/updating data:", error);
      }
    };

    const clearForm = () => {
      formData.value = {};
    };

    // Access the id from the query parameters and fetch corresponding data
    const { id } = router.currentRoute.value.query;
    onMounted(async () => {
      if (id) {
        try {
          const response = await axios.get(
            `http://localhost:3000/Employee/${id}`
          );
          const employeeData = response.data;
          formData.value = { ...employeeData };
        } catch (error) {
          console.error("Error fetching employee data:", error);
        }
      }
    });

    return {
      formData,
      submitForm,
      clearForm,
    };
  },
};