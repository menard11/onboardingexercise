import { onMounted, ref } from "vue";
import axios from "axios";
import Swal from "sweetalert2";
import { useRouter } from "vue-router";
import jsPDF from 'jspdf';
import 'jspdf-autotable';
export default {
  setup() {
    const router = useRouter();
    const rows = ref([]);
    const columns = ref([
      {
        name: "EmployeeID",
        label: "Employee ID",
        align: "left",
        field: "EmployeeID",
      },

      {
        name: "name",
        label: "Name",
        align: "left",
        field: "name",
      },

      {
        name: "email",
        label: "Email",
        align: "left",
        field: "email",
      },

      {
        name: "status",
        label: "Status",
        align: "left",
        field: "status",
      },

      {
        name: "address",
        label: "Address",
        align: "left",
        field: "address",
      },
    ]);

    let selectedRow = ref({});

    const getTodos = () => {
      axios.get("http://localhost:3000/Employee").then((response) => {
        rows.value = response.data;
      });
    };

    const toggleActionDiv = (row) => {
      row.showAction = !row.showAction;
      selectedRow.value = row;
      console.log(selectedRow);
    };

    const deleteEmployee = (row) => {
      axios
        .delete(`http://localhost:3000/Employee/${row.id}`)
        .then((response) => {
          console.log("Data deleted successfully:", response.data);
          Swal.fire({
            icon: "success",
            title: "Success!",
            text: "Data Deleted successfully!",
          });
          getTodos();
        });
    };

    const editEmployee = (row) => {
      router.push({ name: "my-form", query: { id: row.id } });
    };

    onMounted(() => {
      getTodos();
    });

    const downloadPdf = () => {
      const doc = new jsPDF();
      // Define columns for the PDF table
      const headers = columns.value.map(column => column.label);
      // Extract data for each row
      const data = rows.value.map(row => {
        return columns.value.map(column => row[column.field]);
      });

      doc.text('Table List', 100, 10);

      doc.autoTable({
        head: [headers],
        body: data,
        startY: 20,
      });
    
      // Download PDF
      doc.save("Employee List.pdf");
    };

    return {
      columns,
      rows,
      toggleActionDiv,
      selectedRow,
      deleteEmployee,
      editEmployee,
      downloadPdf,
    };
  },
};
